# LDAP Terraform Demo

A demo using HashiCorp Terraform to spin up the most recent GitLab EE on an Ubuntu 18.04 image & a populated OpenLDAP server (also Ubuntu 18.04) to show how LDAP synchronization works.  

## Getting Started

This project creates the following infrastructure on AWS:

* A single OpenLDAP server/VM complete with example data imported
* A single GitLab Enterprise Edition (EE) server/VM
* Creates security groups that:
  * Opens HTTP, HTTPS & SSH ports for the GitLab EE server from anywhere
  * Opens only SSH server from anywhere
  * allows the GitLab EE to connect to the OpenLDAP server via LDAP:// or LDAPS://
  * allow for ICMP between the two EC2 VMs
* Creates a private DNS zone for the LDAP server. By default, this is example.com
* Updates the private zone, as well as the public zone, relevant A / host records.


### Prerequisites

1) [Install Terraform](https://www.terraform.io/downloads.html)  Move the terraform executable into your path/update your path. /usr/local/bin/ works well.
2) Export AWS Keys for an account with programmatic access to AWS in terminal:

>`export AWS_ACCESS_KEY_ID=(your access key id)`

>`export AWS_SECRET_ACCESS_KEY=(your secret access key)`

3) Export your public SSH key to AWS (EC2 Dashboard -> import -> ssh key (typically created locally as id_rsa.pub). )

### Deployment

1) Rename/move .Variables.tf to Variables.tf & update the file for your needs.
2) Rename/move /scripts/.install-GitLab-EE.sh & update to /scripts/install-GitLab-EE.sh
2) In the cloned repo, run:
>`terraform init`

>`terraform plan -out:<dir/filename>`  (The output should not show errors.)

>`terraform apply <dir/filename>` (Apply the terraform plan from the terraform plan stage)

3) Check the status via `terraform show`
  Optionally, view on the [AWS console](https://aws.amazon.com)

                or

  SSH into the VMs via the public IP address listed on the `terraform apply` output.

## Notes

  For Ubuntu 18.04 AMIs on AWS, the default user name is "ubuntu" (minus the quotes.) To avoid confusion, I've added a required step to import the provisioning system's SSH public key into AWS. If you are not sure what that is then please see: https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair


## Directory / File Walk Through

* Variables.tf - **_Modifications are needed to successfully run Terraform. Please read comments & descriptions carefully_**
* OpenLDAP.tf - Terraform configuration file used to create the OpenLDAP server on AWS EC2. Two files are deployed:  
  * scripts/ldap-provision.sh - LDAP provisioning script. If you want to use a different private domain outside of example.com then please update this file as well.
  * data/ldap-info.ldif - LDAP information that will be imported. Variables should match with the private domain that is used. By default, example.com is used.
* GitLab.tf - Terraform configuration file used to install GitLab Enterprise Edition including postfix/email server. **_Note: AWS does not allow EC2 hosts to directly send email. If you want email to work then configure SNS_**
  * scripts/install-GitLab-EE.sh - Install script for GitLabEE. **_Change required for domain name_**
* DNS.tf - Terraform configuration file used to create & manage both the private & public zones for name resolution
* Security_Groups.tf - Terraform configuration file used to open ports externally as well as within the VPC.
* Outputs.tf - Terraform configuration file that will dump the server & IP address information.
