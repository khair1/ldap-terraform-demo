

# Find the Route53 zone ID #
data "aws_route53_zone" "public" {
  name = "${var.GL_Domain}"
  private_zone = false
}

# Name of private domain. Pulled from Variables.tf
resource "aws_route53_zone" "private" {
  name = "${var.LDAP_Domain}"

  vpc {
     vpc_id = "${data.aws_vpc.default.id}"
   }
}

resource "aws_route53_record" "GitlabEE-pub" {
  zone_id = "${data.aws_route53_zone.public.zone_id}"
  name = "${var.GL_Host_Name}${var.GL_Subdomain}${var.GL_Domain}"
  type    = "A"
  ttl = "${var.ttl}"
  records = [
    "${aws_instance.GitLabEE.public_ip}"
  ]

}

resource "aws_route53_record" "OpenLDAP-priv" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "ldap.${aws_route53_zone.private.name}"
  type    = "A"

  records = [
    "${aws_instance.OpenLDAP.private_ip}"
  ]

  ttl = "${var.ttl}"
}
