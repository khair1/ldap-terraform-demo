variable "region" {
 default = "us-east-2"
 description = "The name of the AWS region you'd like to deploy in."
}

variable "availability_zone" {
  default = "us-east-2b"
}

variable "ami" {
  type = "map"
  default = {
    # us-east-2 = "ami-0f65671a86f061fcd"
    ap-northeast-1	= "ami-0fd02119f1653c976"
    ap-south-1	= "ami-01187fe59c07cd350"
    ap-southeast-1	= "ami-0efb24bbbf33a2fd7"
    ca-central-1	= "ami-0388b9f812caf5c3f"
    eu-central-1	= "ami-080d06f90eb293a27"
    eu-west-1	= "ami-02790d1ebf3b5181d"
    sa-east-1	= "ami-04fb8967affdf73b6"
    us-east-1	= "ami-0d2505740b82f7948"
    us-west-1	= "ami-09c5eca75eed8245a"
    eu-north-1	= "ami-10bb356e"
    cn-north-1	= "ami-05596fb52c3802012"
    cn-northwest-1	= "ami-03f7db8b059795736"
    us-gov-east-1	= "ami-825fb8f3"
    us-gov-west-1	= "ami-b44b2fd5"
    ap-northeast-2	= "ami-096560874cb404a4d"
    ap-southeast-2	= "ami-03932cb7d3a1a69af"
    eu-west-2	= "ami-06328f1e652dc7605"
    us-east-2	= "ami-0cf8cc36b8c81c6de"
    us-west-2	= "ami-0f05ad41860678734"
    ap-northeast-3 ="ami-064d6dc91cdb4daa8"
    eu-west-3 =	"ami-0697abcfa8916e673"

  }
  description = "Using Ubuntu 18.04. This is a list of AMIs by AWS region. Builds 20181124"
}

# Using the default VPC in the selected region for this demo.
data "aws_vpc" "default" {
  default = true
}

variable "ssh_key" {
  default = "kelly-mbp"
  description = "This is the name of your provisioning machine's public SSH key"
}

variable "GitLab_friendly_name" {
  default = "KH-GitLabEE"
  description = "This is how the GitLab Omnibus install will show in AWS' EC2 console"
}

variable "GitLabEE_EC2_instance_type" {
  default = "t2.large"
}

variable "OpenLDAP_EC2_instance_type" {
  default = "t2.medium"
}

variable "OpenLDAP_friendly_name" {
  default = "KH-OpenLDAP"
  description = "This is how the OpenLDAP instance will show in AWS' EC2 console"
}

variable "SG-Prefix" {
  default = "KH-OpenLDAP_Demo-"
  description = "Prefix to add to security groups"
}

variable "ttl" {
  default = 30
  description = "Default DNS time to live for a record. Variable set to 30 seconds"
}

variable "private_ssh_key_dir" {
  default = "/Users/kelly/.ssh/id_rsa"
  description = "Location of the PRIVATE key provisioning infrastructure."
}

variable "LDAP_Domain" {
  default = "example.com."
}

variable "GL_Domain" {
  default = "gl-demo.io."
}

variable "GL_Subdomain" {
  default = "kh.sa."
  # default = ""
}

variable "GL_Host_Name" {
  default = "gitlab."
}

