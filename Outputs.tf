output "Public IP address of GitLabEE" {
  value = "${aws_instance.GitLabEE.public_ip}"
}

output "Private IP address of GitLabEE" {
  value = "${aws_instance.GitLabEE.private_ip}"
}

output "FQDN of the LDAP server" {
  value = "${aws_route53_record.OpenLDAP-priv.name}"
}

output "Private IP address of LDAP server" {
  value = "${aws_instance.OpenLDAP.private_ip}"
}
