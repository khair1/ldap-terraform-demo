#!/bin/bash
set -eux

config_organization_name=Example
# config_fqdn=$(hostname --fqdn)
# config_domain=$(hostname --domain)
# config_domain_dc="dc=$(echo $config_domain | sed 's/\./,dc=/g')"
# config_admin_dn="cn=admin,$config_domain_dc"
config_fqdn=ldap.example.com
config_domain=example.com
config_domain_dc="dc=example,dc=com"
config_admin_dn="cn=admin,dc=example,dc=com"
config_admin_password=password

echo "127.0.0.1 $config_fqdn" >>/etc/hosts

apt-get install -y --no-install-recommends vim
cat >/etc/vim/vimrc.local <<'EOF'
syntax on
set background=dark
set esckeys
set ruler
set laststatus=2
set nobackup
autocmd BufNewFile,BufRead Vagrantfile set ft=ruby
EOF

# these anwsers were obtained (after installing slapd) with:
#
#   #sudo debconf-show slapd
#   sudo apt-get install debconf-utils
#   # this way you can see the comments:
#   sudo debconf-get-selections
#   # this way you can just see the values needed for debconf-set-selections:
#   sudo debconf-get-selections | grep -E '^slapd\s+' | sort
debconf-set-selections <<EOF
slapd slapd/password1 password $config_admin_password
slapd slapd/password2 password $config_admin_password
slapd slapd/domain string $config_domain
slapd shared/organization string $config_organization_name
EOF

apt-get install -y --no-install-recommends slapd ldap-utils

# create the people container.
# NB the `cn=admin,$config_domain_dc` user was automatically created
#    when the slapd package was installed.
#ldapadd -c -D $config_admin_dn -w $config_admin_password <<EOF
#dn: ou=people,$config_domain_dc
#objectClass: organizationalUnit
#ou: people
#EOF


ldapadd -x -D "cn=admin,dc=example,dc=com" -w password -H ldap:// -f /tmp/ldap-info.ldif

# show the configuration tree.
ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config dn | grep -v '^$'

# show the data tree.
ldapsearch -x -LLL -b $config_domain_dc dn | grep -v '^$'

# search for people and print some of their attributes.
ldapsearch -x -LLL -b $config_domain_dc '(objectClass=person)' cn mail
