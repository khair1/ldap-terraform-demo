provider "aws" {
        region     = "${var.region}"
}

data "aws_ami" "OpenLDAP" {
  most_recent      = true

  filter {
    name   = "name"
    values = ["*KH-OpenLDAP*"]
  }

  filter {
    name = "tag:Button_Pusher"
    values =  ["*khair@gitlab.com*"]
  }
  #
  # name_regex = "^ami-\\d{3}"
  owners     =  ["self"]
}


resource "aws_instance" "OpenLDAP" {
  ami =  "${data.aws_ami.OpenLDAP.id}"
  instance_type = "${var.OpenLDAP_EC2_instance_type}"
  vpc_security_group_ids = [
    "${aws_security_group.SSH_Only.id}",
    "${aws_security_group.Allow_LDAP.id}",
    "${aws_security_group.Allow_ICMP_LDAP.id}"
    ]
  key_name = "${var.ssh_key}"
  tags {
      Name = "${var.OpenLDAP_friendly_name}"
    }
    provisioner "file" {
      source      = "scripts/ldap-provision.sh"
      destination = "/tmp/ldap-provision.sh"
    }

    provisioner "file" {
      source      = "data/ldap-info.ldif"
      destination = "/tmp/ldap-info.ldif"
    }

   provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/ldap-provision.sh",
      "sudo /tmp/ldap-provision.sh",
      ]
   }

   connection {
   user = "ubuntu"
   private_key = "${file("${var.private_ssh_key_dir}")}"

   }
}
